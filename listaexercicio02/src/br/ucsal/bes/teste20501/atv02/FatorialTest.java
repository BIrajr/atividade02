package br.ucsal.bes.teste20501.atv02;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigInteger;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FatorialTest {
	Fatorial fatorial;

	@Before
	public void setUp() throws Exception {
		fatorial = new Fatorial();
	}

	@Test
	public void testEntrada() throws ValorNaoValidoException {
		ByteArrayInputStream in = new ByteArrayInputStream("8".getBytes());
		System.setIn(in);
		int resultadoAtual = fatorial.entrada();
		int resultadoEsperado = 8;
		Assert.assertEquals(resultadoEsperado, resultadoAtual);
		in = new ByteArrayInputStream("0".getBytes());
		System.setIn(in);
		resultadoAtual = fatorial.entrada();
		resultadoEsperado = 0;
		Assert.assertEquals(resultadoEsperado, resultadoAtual);
		in = new ByteArrayInputStream("100".getBytes());
		System.setIn(in);
		resultadoAtual = fatorial.entrada();
		resultadoEsperado = 100;
		Assert.assertEquals(resultadoEsperado, resultadoAtual);

	}

	@SuppressWarnings("static-access")
	@Test
	public void testCalculo() {
		BigInteger num = BigInteger.valueOf(2);
		BigInteger resultadoAtual = fatorial.calculo(num);
		BigInteger resultadoEsperado = BigInteger.valueOf(2);
		Assert.assertEquals(resultadoEsperado, resultadoAtual);
		num = BigInteger.valueOf(3);
		resultadoAtual = fatorial.calculo(num);
		resultadoEsperado = BigInteger.valueOf(6);
		Assert.assertEquals(resultadoEsperado, resultadoAtual);
		num = BigInteger.valueOf(4);
		resultadoAtual = fatorial.calculo(num);
		resultadoEsperado = BigInteger.valueOf(24);
		Assert.assertEquals(resultadoEsperado, resultadoAtual);
		num = BigInteger.valueOf(5);
		resultadoAtual = fatorial.calculo(num);
		resultadoEsperado = BigInteger.valueOf(120);
		Assert.assertEquals(resultadoEsperado, resultadoAtual);
		num = BigInteger.valueOf(6);
		resultadoAtual = fatorial.calculo(num);
		resultadoEsperado = BigInteger.valueOf(720);
		Assert.assertEquals(resultadoEsperado, resultadoAtual);
		num = BigInteger.valueOf(7);
		resultadoAtual = fatorial.calculo(num);
		resultadoEsperado = BigInteger.valueOf(5040);
		Assert.assertEquals(resultadoEsperado, resultadoAtual);
		num = BigInteger.valueOf(8);
		resultadoAtual = fatorial.calculo(num);
		resultadoEsperado = BigInteger.valueOf(40320);
		Assert.assertEquals(resultadoEsperado, resultadoAtual);
		num = BigInteger.valueOf(10);
		resultadoAtual = fatorial.calculo(num);
		resultadoEsperado = BigInteger.valueOf(3628800);
		Assert.assertEquals(resultadoEsperado, resultadoAtual);
		num = BigInteger.valueOf(0);
		resultadoAtual = fatorial.calculo(num);
		resultadoEsperado = BigInteger.valueOf(1);
		Assert.assertEquals(resultadoEsperado, resultadoAtual);
		num = BigInteger.valueOf(1);
		resultadoAtual = fatorial.calculo(num);
		resultadoEsperado = BigInteger.valueOf(1);
		Assert.assertEquals(resultadoEsperado, resultadoAtual);

	}

	@SuppressWarnings("static-access")
	@Test
	public void testSaida() {
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		System.setOut(new PrintStream(out));

		Fatorial fatorial = new Fatorial();
		fatorial.saida(BigInteger.valueOf(0));
		String resultadoAtual = out.toString();
		String resultadoEsperado = "Resultado: 0\n\r";
		Assert.assertEquals(resultadoEsperado, resultadoAtual);
	}

}
