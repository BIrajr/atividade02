package br.ucsal.bes.teste20501.atv02;

import java.math.BigInteger;
import java.util.Scanner;

public class Fatorial {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		int aux;
		boolean error = false;
		do {
			try {
				aux = entrada();
				BigInteger num = BigInteger.valueOf(aux);
				saida(calculo(num));
			} catch (ValorNaoValidoException e) {
				System.out.println(
						"N�mero inv�lido, tentou quebrar minha aplica��o n� rapaz? Usu�rios... Por favor digite um numero entre 0 e 100");
				error = true;
			}
		} while (error == true);
	}

	public static int entrada() throws ValorNaoValidoException {
		System.out.println("Digite um numero entre 0 e 100:");
		int num = sc.nextInt();
		validarNumero(num);
		return num;
	}

	private static void validarNumero(int num) throws ValorNaoValidoException {
		if (num < 0 || num > 100) {
			throw new ValorNaoValidoException();
		}

	}

	public static BigInteger calculo(BigInteger num) {
		if (num.intValue() == 0) {
			return BigInteger.ONE;
		}
		for (int i = num.intValue() - 1; i > 0; i--) {
			num = num.multiply(BigInteger.valueOf(i));
		}

		return num;
	}

	public static void saida(BigInteger num) {
		System.out.println("Resultado: " + num);
	}
}
